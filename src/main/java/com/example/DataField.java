package com.example;

public interface DataField {
    String getID();
    String getName();
}
