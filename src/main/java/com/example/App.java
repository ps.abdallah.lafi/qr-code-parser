package com.example;

import java.util.HashMap;

public class App {
    public static void main(String[] args) {
        QRProcessor processor = QRProcessor.getInstance();
        String QRCode = "00020101021228270004CliQ0108TESTJOA00203002520412345303400540515.1055020256050.9005802JO5910Merchant026005Amman610511118628001030020306store20404loy20510QRTest02540605cust20704ter20804JOQR100500222110340064180002FR0108Marchand80310004JOQR01192021-04-05T10:47:5763041E63";
        HashMap<String, String> qrCodeDetails = processor.getDetails(QRCode);
        qrCodeDetails.forEach((key, value) -> System.out.println(key + ": " + value));
    }
}
