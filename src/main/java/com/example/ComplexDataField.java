package com.example;

import java.util.ArrayList;
import java.util.HashMap;

public class ComplexDataField implements DataField {

    private final String name;
    private final String ID;
    private final HashMap<String, DataField> dataFields;

    public ComplexDataField(String name, String ID) {
        this.name = name;
        this.ID = ID;
        dataFields = new HashMap<>();
    }


    @Override
    public String getID() {
        return ID;
    }

    @Override
    public String getName() {
        return name;
    }

    public void addDataField(DataField dataField) {
        dataFields.put(dataField.getID(), dataField);
    }

    public boolean contains(DataField dataField) {
        return dataFields.containsValue(dataField);
    }

    public String getInnerName(String id) {
        if (!dataFields.containsKey(id))
            throw new IllegalArgumentException("An object with such ID does not exist");
        return dataFields.get(id).getName();
    }
}
