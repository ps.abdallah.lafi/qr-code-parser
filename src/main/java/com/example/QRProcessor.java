package com.example;

import java.util.HashMap;

public class QRProcessor {

    private static QRProcessor instance;

    private static final int CRC_LENGTH = 4;
    private static final int ID_OFFSET = 2;
    private static final int LENGTH_OFFSET = 4;

    private QRProcessor() {
    }

    public static QRProcessor getInstance() {
        if (instance == null)
            instance = new QRProcessor();
        return instance;
    }

    public HashMap<String, String> getDetails(String qrCode) {
        if (!validateQR(qrCode))
            throw new IllegalArgumentException("Invalid CRC");
        HashMap<String, String> parsedData = new HashMap<>();
        QRDataObjects qrDataObjects = QRDataObjects.getInstance();
        HashMap<String, DataField> qrDataMap = qrDataObjects.getQRMap();
        String currentID;
        int currentLength;
        for (int currentIndex = 0; currentIndex < qrCode.length(); ) {
            currentID = qrCode.substring(currentIndex, currentIndex + ID_OFFSET);
            currentLength = Integer.parseInt(qrCode.substring(currentIndex + ID_OFFSET, currentIndex + LENGTH_OFFSET));
            if (qrDataMap.get(currentID) instanceof ComplexDataField) {
                for (int innerIndex = LENGTH_OFFSET; innerIndex < currentLength; ) {
                    String subID = qrCode.substring(currentIndex + innerIndex, currentIndex + innerIndex + ID_OFFSET);
                    int subLength = Integer.parseInt(qrCode.substring(currentIndex + innerIndex + ID_OFFSET, currentIndex + innerIndex + LENGTH_OFFSET));
                    String fieldValue = qrCode.substring(currentIndex + innerIndex + LENGTH_OFFSET, currentIndex + innerIndex + subLength + LENGTH_OFFSET);
                    String fieldName = ((ComplexDataField) qrDataMap.get(currentID)).getInnerName(subID);
                    parsedData.put(fieldName, fieldValue);
                    innerIndex += LENGTH_OFFSET + subLength;
                }
                currentIndex += LENGTH_OFFSET + currentLength;
            } else {
                String fieldValue = qrCode.substring(currentIndex + LENGTH_OFFSET, currentIndex + currentLength + LENGTH_OFFSET);
                String fieldName = qrDataMap.get(currentID).getName();
                if (fieldName.equals("transaction currency"))
                    fieldValue = CurrencyUtil.getCurrencyName(Integer.parseInt(fieldValue));
                parsedData.put(fieldName, fieldValue);
                currentIndex += LENGTH_OFFSET + currentLength;
            }
        }

        return parsedData;
    }

    private boolean validateQR(String qrCode) {

        String qrWithoutCRC = qrCode.substring(0, qrCode.length() - CRC_LENGTH);
        String crc = qrCode.substring(qrCode.length() - CRC_LENGTH);
        return CRCChecker.checkCRC(qrWithoutCRC).equalsIgnoreCase(crc);
    }

}
