package com.example;

public class CRCChecker {
    public static String checkCRC(String data) {
        int crc = 0xFFFF;
        for (int i = 0; i < data.length(); ++i) {
            crc ^= data.charAt(i) << 8;
            for (int j = 0; j < 8; ++j) {
                crc = (crc & 0x8000) > 0 ? (crc << 1) ^ 0x1021 : crc << 1;
            }
        }
        return Integer.toHexString(crc & 0xFFFF);

    }
}
