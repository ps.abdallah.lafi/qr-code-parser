package com.example;

import java.util.HashMap;

public class QRDataObjects {
    private static QRDataObjects instance;
    private HashMap<String, DataField> QRObjectsMap;

    private QRDataObjects() {
    }

    public static QRDataObjects getInstance() {
        if (instance == null)
            instance = new QRDataObjects();
        return instance;
    }

    public HashMap<String, DataField> getQRMap() {
        if (QRObjectsMap == null)
            createQRMap();
        return QRObjectsMap;
    }

    private void createQRMap() {
        QRObjectsMap = new HashMap<>();
        PrimitiveDataField id = new PrimitiveDataField("id", "00");
        PrimitiveDataField method = new PrimitiveDataField("method", "01");

        ComplexDataField merchant = new ComplexDataField("merchant account information", "28");
        PrimitiveDataField switchGUID = new PrimitiveDataField("merchant switch GUID", "00");
        PrimitiveDataField acquirerID = new PrimitiveDataField("merchant acquirer id", "01");
        PrimitiveDataField accountID = new PrimitiveDataField("merchant account id", "02");
        merchant.addDataField(switchGUID);
        merchant.addDataField(acquirerID);
        merchant.addDataField(accountID);

        PrimitiveDataField MCC = new PrimitiveDataField("merchant category code", "52");
        PrimitiveDataField transactionCurrency = new PrimitiveDataField("transaction currency", "53");
        PrimitiveDataField transactionAmount = new PrimitiveDataField("transaction amount", "54");
        PrimitiveDataField tip = new PrimitiveDataField("tip", "55");
        PrimitiveDataField fixedConvience = new PrimitiveDataField("fixed convenience", "56");
        PrimitiveDataField percentageConvience = new PrimitiveDataField("percentage convenience", "57");
        PrimitiveDataField countryCode = new PrimitiveDataField("country code", "58");
        PrimitiveDataField merchantName = new PrimitiveDataField("merchant name", "59");
        PrimitiveDataField merchantCity = new PrimitiveDataField("merchant city", "60");
        PrimitiveDataField postalCode = new PrimitiveDataField("postal code", "61");

        ComplexDataField additionalData = new ComplexDataField("additional data", "62");
        PrimitiveDataField billNumber = new PrimitiveDataField("bill number", "01");
        PrimitiveDataField mobileNumber = new PrimitiveDataField("mobile number", "02");
        PrimitiveDataField storeLabel = new PrimitiveDataField("store label", "03");
        PrimitiveDataField loyaltyNumber = new PrimitiveDataField("loyalty number", "04");
        PrimitiveDataField referenceLabel = new PrimitiveDataField("reference label", "05");
        PrimitiveDataField customerLabel = new PrimitiveDataField("customer label", "06");
        PrimitiveDataField terminalLabel = new PrimitiveDataField("terminal label", "07");
        PrimitiveDataField purposeOfTransaction = new PrimitiveDataField("purpose of transaction", "08");
        PrimitiveDataField customerDataRequest = new PrimitiveDataField("additional customer data request", "09");
        PrimitiveDataField merchantTaxInformation = new PrimitiveDataField("merchant tax information", "10");
        PrimitiveDataField merchantChannel = new PrimitiveDataField("merchant channel", "11");
        additionalData.addDataField(billNumber);
        additionalData.addDataField(mobileNumber);
        additionalData.addDataField(storeLabel);
        additionalData.addDataField(loyaltyNumber);
        additionalData.addDataField(referenceLabel);
        additionalData.addDataField(customerLabel);
        additionalData.addDataField(terminalLabel);
        additionalData.addDataField(purposeOfTransaction);
        additionalData.addDataField(customerDataRequest);
        additionalData.addDataField(merchantTaxInformation);
        additionalData.addDataField(merchantChannel);

        PrimitiveDataField CRC = new PrimitiveDataField("cyclic dependency check", "63");
        PrimitiveDataField additionalLanguageTemplate = new PrimitiveDataField("additional language template", "64");

        ComplexDataField dateTime = new ComplexDataField("date and time", "80");
        PrimitiveDataField dateGUID = new PrimitiveDataField("qr date GUID", "00");
        PrimitiveDataField date = new PrimitiveDataField("qr date", "01");
        dateTime.addDataField(dateGUID);
        dateTime.addDataField(date);

        ComplexDataField verificationCode = new ComplexDataField("verification code", "81");
        PrimitiveDataField verificationCodeGUID = new PrimitiveDataField("verification code GUID", "00");
        PrimitiveDataField pinOTP = new PrimitiveDataField("pin or OTP", "01");
        verificationCode.addDataField(verificationCodeGUID);
        verificationCode.addDataField(pinOTP);

        ComplexDataField location = new ComplexDataField("location", "82");
        PrimitiveDataField locationGUID = new PrimitiveDataField("location GUID", "00");
        PrimitiveDataField locationCoordinates = new PrimitiveDataField("location", "01");
        location.addDataField(locationGUID);
        location.addDataField(locationCoordinates);


        QRObjectsMap.put(id.getID(), id);
        QRObjectsMap.put(method.getID(), method);
        QRObjectsMap.put(merchant.getID(), merchant);
        QRObjectsMap.put(MCC.getID(), MCC);
        QRObjectsMap.put(transactionCurrency.getID(), transactionCurrency);
        QRObjectsMap.put(transactionAmount.getID(), transactionAmount);
        QRObjectsMap.put(tip.getID(), tip);
        QRObjectsMap.put(fixedConvience.getID(), fixedConvience);
        QRObjectsMap.put(percentageConvience.getID(), percentageConvience);
        QRObjectsMap.put(countryCode.getID(), countryCode);
        QRObjectsMap.put(merchantName.getID(), merchantName);
        QRObjectsMap.put(merchantCity.getID(), merchantCity);
        QRObjectsMap.put(postalCode.getID(), postalCode);
        QRObjectsMap.put(additionalData.getID(), additionalData);
        QRObjectsMap.put(CRC.getID(), CRC);
        QRObjectsMap.put(additionalLanguageTemplate.getID(), additionalLanguageTemplate);
        QRObjectsMap.put(dateTime.getID(), dateTime);
        QRObjectsMap.put(verificationCode.getID(), verificationCode);
        QRObjectsMap.put(location.getID(), location);

    }
}
