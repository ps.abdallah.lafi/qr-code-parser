package com.example;

import java.util.Currency;
import java.util.Set;

public class CurrencyUtil {
    public static String getCurrencyName(int currencyCode) {

        Set<Currency> currencies = Currency.getAvailableCurrencies();
        for (Currency currency : currencies) {
            if (currency.getNumericCode() == currencyCode) {
                return currency.getCurrencyCode();
            }
        }
        throw new IllegalArgumentException("A currency with such code doesn't exist");
    }
}
