package com.example;


public class PrimitiveDataField implements DataField {

    private final String name;
    private final String ID;

    public PrimitiveDataField(String name, String ID) {
        this.name = name;
        this.ID = ID;
    }

    @Override
    public String getID() {
        return ID;
    }

    @Override
    public String getName() {
        return name;
    }
}
