package com.example;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CurrencyUtilTest {

    @Test
    void givenCurrencyCode_whenGetCurrencyName_returnCurrencyName(){
        String currencyName = CurrencyUtil.getCurrencyName(400);
        assertEquals("JOD",currencyName);
    }

    @Test
    void givenInvalidCurrencyCode_whenGetCurrencyName_throwException() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> CurrencyUtil.getCurrencyName(34123));
        assertEquals("A currency with such code doesn't exist", exception.getMessage());
    }
}
