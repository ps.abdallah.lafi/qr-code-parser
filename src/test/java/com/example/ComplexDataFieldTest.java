package com.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ComplexDataFieldTest {

    @Test
    void givenDataField_whenAddDataField_thenAddDataField() {
        ComplexDataField merchantAccountInformation = new ComplexDataField("merchant account information", "28");
        PrimitiveDataField switchGUID = new PrimitiveDataField("merchant switch GUID", "00");
        merchantAccountInformation.addDataField(switchGUID);
        assertTrue(merchantAccountInformation.contains(switchGUID));
    }

    @Test
    void givenInvalidInnerDataID_whenGetInnerName_thenThrowException(){
        ComplexDataField merchant = new ComplexDataField("merchant account information", "28");
        PrimitiveDataField switchGUID = new PrimitiveDataField("merchant switch GUID", "00");
        merchant.addDataField(switchGUID);
        Exception exception = assertThrows(IllegalArgumentException.class, ()->merchant.getInnerName("01"));
        assertEquals("An object with such ID does not exist",exception.getMessage());
    }
    @Test
    void givenInnerDataID_whenGetInnerName_thenReturnName() {
        ComplexDataField merchant = new ComplexDataField("merchant account information", "28");
        PrimitiveDataField switchGUID = new PrimitiveDataField("merchant switch GUID", "00");
        merchant.addDataField(switchGUID);
        assertEquals("merchant switch GUID", merchant.getInnerName("00"));
    }
}
