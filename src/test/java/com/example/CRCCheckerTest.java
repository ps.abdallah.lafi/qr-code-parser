package com.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CRCCheckerTest {

    @Test
    void givenString_whenCheckCRC_thenReturnCRC() {
        String testString = "00020101021228280004CliQ0108TESTJOA002040001520412345303400540511.355502015802JO5913Merchant 00016005Amman6105111186263010400010306store10406loyal10510QRTest01540609customer10804JOQR80310004JOQR01192021-04-05T10:44:596304";
        String crcValue = CRCChecker.checkCRC(testString);
        Assertions.assertEquals("b56c", crcValue);
    }

}
