package com.example;

import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

public class QRProcessorTest {

    @Test
    void givenQRCodeWithValidCRC_whenProcess_thenReturnDetails() {
        String QRCode = "00020101021228270004CliQ0108TESTJOA00203002520412345303400540515.1055020256050.9005802JO5910Merchant026005Amman610511118628001030020306store20404loy20510QRTest02540605cust20704ter20804JOQR100500222110340064180002FR0108Marchand80310004JOQR01192021-04-05T10:47:5763041E63";
        QRProcessor processor = QRProcessor.getInstance();
        HashMap<String, String> details = processor.getDetails(QRCode);

        assertTrue(details.containsKey("id"));
        assertEquals("01", details.get("id"));
        assertTrue(details.containsKey("method"));
        assertEquals("12", details.get("method"));

        assertMerchantInfo(details);

        assertTransactionInfo(details);

        assertLocationInfo(details);

        assertTimeInfo(details);

        assertTrue(details.containsKey("bill number"));
        assertEquals("002", details.get("bill number"));
        assertTrue(details.containsKey("store label"));
        assertEquals("store2", details.get("store label"));
        assertTrue(details.containsKey("loyalty number"));
        assertEquals("loy2", details.get("loyalty number"));
        assertTrue(details.containsKey("reference label"));
        assertEquals("QRTest0254", details.get("reference label"));
        assertTrue(details.containsKey("customer label"));
        assertEquals("cust2", details.get("customer label"));
        assertTrue(details.containsKey("terminal label"));
        assertEquals("ter2", details.get("terminal label"));
        assertTrue(details.containsKey("purpose of transaction"));
        assertEquals("JOQR", details.get("purpose of transaction"));
        assertTrue(details.containsKey("cyclic dependency check"));
        assertEquals("1E63", details.get("cyclic dependency check"));
        assertTrue(details.containsKey("additional language template"));
        assertEquals("0002FR0108Marchand", details.get("additional language template"));
    }

    private void assertTimeInfo(HashMap<String, String> details) {
        assertTrue(details.containsKey("qr date GUID"));
        assertEquals("JOQR", details.get("qr date GUID"));
        assertTrue(details.containsKey("qr date"));
        assertEquals("2021-04-05T10:47:57", details.get("qr date"));
    }

    private void assertLocationInfo(HashMap<String, String> details) {
        assertTrue(details.containsKey("country code"));
        assertEquals("JO", details.get("country code"));
        assertTrue(details.containsKey("postal code"));
        assertEquals("11118", details.get("postal code"));
    }

    private void assertTransactionInfo(HashMap<String, String> details) {
        assertTrue(details.containsKey("transaction currency"));
        assertEquals("JOD", details.get("transaction currency"));
        assertTrue(details.containsKey("transaction amount"));
        assertEquals("15.10", details.get("transaction amount"));
        assertTrue(details.containsKey("tip"));
        assertEquals("02", details.get("tip"));
        assertTrue(details.containsKey("fixed convenience"));
        assertEquals("0.900", details.get("fixed convenience"));
    }

    private void assertMerchantInfo(HashMap<String, String> details) {
        assertTrue(details.containsKey("merchant switch GUID"));
        assertEquals("CliQ", details.get("merchant switch GUID"));
        assertTrue(details.containsKey("merchant acquirer id"));
        assertEquals("TESTJOA0", details.get("merchant acquirer id"));
        assertTrue(details.containsKey("merchant account id"));
        assertEquals("002", details.get("merchant account id"));
        assertTrue(details.containsKey("merchant category code"));
        assertEquals("1234", details.get("merchant category code"));
        assertTrue(details.containsKey("merchant name"));
        assertEquals("Merchant02", details.get("merchant name"));
        assertTrue(details.containsKey("merchant city"));
        assertEquals("Amman", details.get("merchant city"));
        assertTrue(details.containsKey("merchant tax information"));
        assertEquals("00222", details.get("merchant tax information"));
        assertTrue(details.containsKey("merchant channel"));
        assertEquals("400", details.get("merchant channel"));


    }

    @Test
    void givenQRCodeWithInvalidCRC_whenProcess_thenThrowException() {
        String QRCode = "00020101021226280005JOMOP0115ARABJO00000500128540004CliQ0108ARABJOA00230JO61ARAB134000000013403179510052045122530340054056.5235802JO5915AB Grand Stores6005Amman6261010722876910315ARABJO000005001050722876910708005001010804JOQR80310004JOQR01192021-02-20T10:05:236304FA5A";
        QRProcessor processor = QRProcessor.getInstance();
        Exception exception = assertThrows(IllegalArgumentException.class, () -> processor.getDetails(QRCode));
        assertEquals("Invalid CRC", exception.getMessage());
    }
}
